class CreateRaces < ActiveRecord::Migration[5.0]
  def change
    create_table :races, :id => false do |t|
      t.integer :id
      t.string :race

      t.timestamps
    end
  end
end
